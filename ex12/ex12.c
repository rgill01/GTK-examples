#include <cairo.h>
#include <gtk/gtk.h>

struct {
	gboolean timer;
	gdouble	 alpha;
	gdouble  size;
	gushort  count;
} glob;

void 
do_drawing(cairo_t *cr, 
	  			 GtkWidget *widget)
{
	cairo_text_extents_t extents;
	gint width, height;

	width = gtk_widget_get_allocated_width(widget);
	height = gtk_widget_get_allocated_height(widget);

	gint x = width/2;
	gint y = height/2;

	cairo_set_source_rgb(cr, 0.5, 0, 0);
	cairo_paint(cr);

	cairo_select_font_face(cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);

	glob.size += 0.8;
	if (glob.size > 20)
		glob.alpha -= 0.01;

	cairo_set_font_size(cr, glob.size);
	cairo_set_source_rgb(cr, 1, 1, 1);

	cairo_text_extents(cr, "Hello World!", &extents);
	cairo_move_to(cr, x - extents.width/2, y + extents.height/2);
	cairo_text_path(cr, "Hello World!");
	// cairo_show_text(cr, "Hello World!");
	cairo_clip(cr);

	cairo_paint_with_alpha(cr, glob.alpha);

	if (glob.alpha <= 0)
	{
		//glob.timer = FALSE;
		glob.size = 1;
		glob.alpha = 1;
	}
}

static gboolean
on_draw_event(GtkWidget *widget,
							cairo_t *cr,
							gpointer user_data)
{
	do_drawing(cr, widget);

	return FALSE;
}

static gboolean
timer_handler(GtkWidget *widget)
{
	if (!glob.timer || widget == NULL ) return FALSE;

	gtk_widget_queue_draw(widget); //widget is darea
	return TRUE;
}

static gboolean
on_draw_event2(GtkWidget *widget,
							cairo_t *cr,
							gpointer user_data)
{
	static gdouble const trs[8][8] = {
      { 0.0, 0.15, 0.30, 0.5, 0.65, 0.80, 0.9, 1.0 },
      { 1.0, 0.0,  0.15, 0.30, 0.5, 0.65, 0.8, 0.9 },
      { 0.9, 1.0,  0.0,  0.15, 0.3, 0.5, 0.65, 0.8 },
      { 0.8, 0.9,  1.0,  0.0,  0.15, 0.3, 0.5, 0.65},
      { 0.65, 0.8, 0.9,  1.0,  0.0,  0.15, 0.3, 0.5 },
      { 0.5, 0.65, 0.8, 0.9, 1.0,  0.0,  0.15, 0.3 },
      { 0.3, 0.5, 0.65, 0.8, 0.9, 1.0,  0.0,  0.15 },
      { 0.15, 0.3, 0.5, 0.65, 0.8, 0.9, 1.0,  0.0, }
  };

  gint width, height;

	width = gtk_widget_get_allocated_width(widget);
	height = gtk_widget_get_allocated_height(widget);

  cairo_translate(cr, width/2, height/2);

  gint i = 0;
  for (i = 0; i < 8; i++) {
      cairo_set_line_width(cr, 3);
      cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
      cairo_set_source_rgba(cr, 0, 0, 0, trs[glob.count%8][i]);

      cairo_move_to(cr, 0.0, -10.0);
      cairo_line_to(cr, 0.0, -40.0);
      cairo_rotate(cr, G_PI/4);

      cairo_stroke(cr);
  }   

	return FALSE;
}


static gboolean
timer_handler2(GtkWidget *widget)
{
	if (!glob.timer || widget == NULL ) return FALSE;

	glob.count += 1;

	gtk_widget_queue_draw(widget); //widget is window
	
	return TRUE;
}

static void 
activate (GtkApplication *app, 
					gpointer 				user_data)
{
	GtkWidget *window;
	GtkWidget *darea, *darea2;

	window = gtk_application_window_new(app);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 1000, 400);
	gtk_window_set_title(GTK_WINDOW(window), "GTK Window");

	darea = gtk_drawing_area_new();
	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);
	g_timeout_add(14, (GSourceFunc) timer_handler, (gpointer) darea);

	darea2 = gtk_drawing_area_new();
	g_signal_connect(G_OBJECT(darea2), "draw", G_CALLBACK(on_draw_event2), NULL);
	g_timeout_add(100, (GSourceFunc) G_CALLBACK(timer_handler2), (gpointer) darea2);
	
	GtkWidget *grid = gtk_grid_new();
	gtk_grid_set_column_spacing (GTK_GRID (grid), 10);
  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);
  gtk_grid_attach (GTK_GRID (grid), darea,  0, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), darea2, 1, 0, 1, 1);
	gtk_container_add(GTK_CONTAINER(window), grid);

	gtk_widget_set_vexpand(darea, TRUE);
	gtk_widget_set_vexpand(darea2, TRUE);

	gtk_widget_show_all(window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  glob.timer = TRUE;
  glob.alpha = 1;
  glob.size = 1;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}