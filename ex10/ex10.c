#include <cairo.h>
#include <gtk/gtk.h>

/* Drawing *on widgets* outside of their rendering cycle is not possible
in GTK+ 3.x.

If you want to draw something off screen and then composite it inside
your widget, you'll need to create a Cairo surface yourself, using
something like: 
*/

gchar buf[256];

gboolean draw_callback(GtkWidget *widget, cairo_t *cr, gpointer data)
{
	GdkRGBA color;
	GtkStyleContext *context;
	guint width, height;

	context = gtk_widget_get_style_context(widget);
	width = gtk_widget_get_allocated_width(widget);
	height = gtk_widget_get_allocated_height(widget);

	gtk_render_background(context, cr, 0, 0, width, height);
	cairo_arc(cr, width/2.0, height/2.0, MIN(width, height) / 2.0, 0, 2*G_PI);
	gtk_style_context_get_color(context, gtk_style_context_get_state(context), &color);
	gdk_cairo_set_source_rgba(cr, &color);

	cairo_fill(cr);

	cairo_move_to(cr, 30, 30);
	cairo_set_font_size(cr, 15);
	cairo_show_text(cr, buf);

	return FALSE;
}

gboolean timer_handler(GtkWidget *widget)
{
	if (widget == NULL) return FALSE;

	GDateTime *now = g_date_time_new_now_local();
	gchar *my_time = g_date_time_format(now, "%H:%M:%S");

	//g_print("%s", my_time);

	g_snprintf(buf, 256, "%s", my_time);

	g_free(my_time);
	g_date_time_unref(now);

	gtk_widget_queue_draw(widget); // widget is window 

	return TRUE;
}

static void activate(GApplication *app, gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *darea;

	window = gtk_application_window_new(GTK_APPLICATION(app));
	gtk_window_set_title(GTK_WINDOW(window), "Timer ex");
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 250);
	gtk_container_set_border_width(GTK_CONTAINER(window), 5);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

	darea = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(window), darea);

	//g_signal_connect(darea, "expose-event", G_CALLBACK(on_expose_event), NULL);
	g_signal_connect(darea, "draw", G_CALLBACK(draw_callback), NULL);
	g_timeout_add(1000, (GSourceFunc) timer_handler, (gpointer) window);

	gtk_widget_show_all(window);
}

int main(int argc, char *argv[])
{
	GtkApplication *app;
	gint status;
	
	app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return status;
}