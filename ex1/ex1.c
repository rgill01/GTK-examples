#include <gtk/gtk.h>

static gboolean asdf;

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  g_print("%d\n", asdf);
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);
  gtk_widget_show_all (window);
}

static int
command_line (GApplication            *application,
              GVariant *options,
              gpointer user_data)
{
  gchar **argv;
  gint argc;
  gint i;

  // argv = g_application_command_line_get_arguments (cmdline, &argc);

  // g_application_command_line_print (cmdline,
                                    // "This text is written back\n"
                                    // "to stdout of the caller\n");

  // for (i = 0; i < argc; i++)
    // g_print ("argument %d: %s\n", i, argv[i]);

  // g_strfreev (argv);

  g_print("%d\n", asdf);

  return -1;
}


int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  // g_signal_connect (app, "handle-local-options", G_CALLBACK (command_line), NULL);
  GOptionEntry e[] = {{
    "asdf",
    0,
    0,

    G_OPTION_ARG_NONE,
    &asdf,

    "hello world!",
    0,
  }, {0}};

  g_application_add_main_option_entries(
    G_APPLICATION(app), e);

  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
