#include <gtk/gtk.h>
#include <glutilities.h>

glutil_text *t;

static gboolean
gl_render(GtkGLArea *area,
					GdkGLContext *context,
					gpointer user_data)
{

	glClear(GL_COLOR_BUFFER_BIT);
	


	glutil_text_draw(t);

	// glFlush();

	g_print("render done\n");
	return FALSE;
}

static void 
gl_realize(GtkWidget *widget,
					 gpointer user_data)
{
	/* 
	You will need to make the GdkGLContext the current context before issuing 
	OpenGL calls; the system sends OpenGL commands to whichever context is 
	current. It is possible to have multiple contexts, so you always need to 
	ensure that the one which you want to draw with is the current one before 
	issuing commands:	
	*/
	gtk_gl_area_make_current(GTK_GL_AREA(widget));

	if (gtk_gl_area_get_error(GTK_GL_AREA(widget)) != NULL)
		return;

	// gtk_gl_area_set_has_depth_buffer(GTK_GL_AREA(widget), GL_TRUE);
	// gtk_gl_area_set_auto_render(GTK_GL_AREA(widget), GL_TRUE);
	// gtk_gl_area_set_has_alpha(GTK_GL_AREA(widget), GL_TRUE);

  // start GLEW extension handler
#ifdef USE_GLEW
  glewExperimental = GL_TRUE;
  if (glewInit()) {
      fprintf(stderr, "Failed to initialize GLEW.\n");
  }
#endif
  // get version info
  const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString(GL_VERSION); // version as a string
  printf("Renderer: %s\n", renderer);
  printf("OpenGL version supported %s\n", version);

	glClearColor (0, 0, 0, 1);
	t = glutil_text_initiate(32);

	glutil_text_render(t, 
		(const glutil_text_data[]) {
			{"Hello World! 0123456789.", 0, 600, 255, 255, 255},
		}, 1, 800, 600);
}

#if 0
static GdkGLContext* 
gl_context(GtkGLArea *area,
					gpointer user_data)
{
	GdkGLContext *context;
	GdkWindow *window;

	GError *err = NULL; 

	window = gtk_widget_get_window(GTK_WIDGET(area));
	context = gdk_window_create_gl_context(window, &err);
	if (err != NULL) {
		fprintf(stderr, "Cannot create context: %s\n", err->message);
		g_error_free(err);
	}
	gdk_gl_context_set_forward_compatible(context, GL_TRUE);
	gdk_gl_context_set_required_version(context, 4, 1);
	gdk_gl_context_set_use_es(context, 1);


	GdkFrameClock *frame_clock = gdk_window_get_frame_clock(window);
	g_signal_connect_swapped(
		frame_clock, "update", G_CALLBACK(gtk_gl_area_queue_render), area);

	return context;
}
#endif

static void 
activate (GtkApplication *app, 
					gpointer 				user_data)
{
	GtkWidget *window;
	GtkWidget *glarea;

	window = gtk_application_window_new(app);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
	gtk_window_set_title(GTK_WINDOW(window), "GTK Window");

	glarea = gtk_gl_area_new();
	g_signal_connect(G_OBJECT(glarea), "realize", G_CALLBACK(gl_realize), NULL);
	g_signal_connect(G_OBJECT(glarea), "render",    G_CALLBACK(gl_render), NULL);
	// g_signal_connect(G_OBJECT(glarea), "create-context", G_CALLBACK(gl_context), NULL);


	gtk_container_add(GTK_CONTAINER(window), glarea);
	gtk_widget_show_all(window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  glutil_text_destroy(t);
  return status;
}