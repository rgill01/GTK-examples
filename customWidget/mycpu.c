/* mycpu.c */

#include "mycpu.h"

/* Properties enum */
enum {
    
   P_0, /* Padding  Property IDs must start from 1, as 0 is reserved for internal use by GObject.  */
   P_PERCENT
};

/* Private data structure */
typedef struct {
  gdouble percent;
  GdkWindow *window;
} My1CpuPrivate;

struct _My1Cpu {
  GtkWidget parent;

  /* Other members, including private data. */

  My1CpuPrivate *priv;   
};


/* Define type */
G_DEFINE_TYPE(My1Cpu, my_1cpu, GTK_TYPE_WIDGET)


const gint WIDTH = 80;
const gint HEIGHT = 100;

/* Internal API */
static void my_1cpu_set_property(GObject *object, guint prop_id, 
    const GValue *value, GParamSpec *pspec);
static void my_1cpu_get_property(GObject *object, guint prop_id,
    GValue *value, GParamSpec *pspec);
static void my_1cpu_preferred_height(GtkWidget *widget, 
    gint *min_height, gint *nat_height);
static void my_1cpu_preferred_width(GtkWidget *widget, 
    gint *min_width, gint *nat_width);
static void my_1cpu_size_allocate(GtkWidget *widget, 
    GtkAllocation *allocation);
static void my_1cpu_realize(GtkWidget *widget);
static gboolean my_draw(GtkWidget *widget, 
    cairo_t *cr);
static gboolean my_1cpu_configure_event(GtkWidget *widget, GdkEventConfigure *event);

/* Initialization */
static void my_1cpu_class_init(My1CpuClass *klass) {
    
   GObjectClass *g_class;
   GtkWidgetClass *w_class;
   GParamSpec *pspec;

   g_class = G_OBJECT_CLASS(klass);
   w_class = GTK_WIDGET_CLASS(klass);

   /* Override widget class methods */
   g_class->set_property  = my_1cpu_set_property;
   g_class->get_property  = my_1cpu_get_property;

   w_class->realize       = my_1cpu_realize;
   w_class->get_preferred_height = my_1cpu_preferred_height;
   w_class->get_preferred_width = my_1cpu_preferred_width;
   w_class->size_allocate = my_1cpu_size_allocate;
   w_class->draw  = my_draw;
   w_class->configure_event = my_1cpu_configure_event;

   /* Install property */
   pspec = g_param_spec_double("percent", "Percent", 
       "What 1CPU load should be displayed", 0, 1, 0, 
       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
       
   g_object_class_install_property(g_class, P_PERCENT, pspec);

   /* Add private data */
   g_type_class_add_private(g_class, sizeof(My1CpuPrivate));
}

static void my_1cpu_init(My1Cpu *cpu) {
    
   My1CpuPrivate *priv;
   
   priv = G_TYPE_INSTANCE_GET_PRIVATE(cpu, MY_TYPE_1CPU, My1CpuPrivate);

   gtk_widget_set_has_window(GTK_WIDGET(cpu), TRUE);

   /* Set default values */
   priv->percent = 0;

   /* Create cache for faster access */
   cpu->priv = priv;
}






/* Overriden virtual methods */
static void my_1cpu_set_property(GObject *object, guint prop_id,
    const GValue *value, GParamSpec *pspec) {
        
   My1Cpu *cpu = MY_1CPU(object);

   switch(prop_id) {
       
      case P_PERCENT:
      
         my_1cpu_set_percent(cpu, g_value_get_double(value));
         break;

      default:
      
         G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
         break;
   }
}

static void my_1cpu_get_property(GObject *object, guint prop_id,
                GValue *value, GParamSpec *pspec) {
                    
   My1Cpu *cpu = MY_1CPU(object);

   switch(prop_id) {
       
      case P_PERCENT:
         g_value_set_double(value, cpu->priv->percent);
         break;

      default:
         G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
         break;
   }
}

static void my_1cpu_realize(GtkWidget *widget) {
    
   My1CpuPrivate *priv = MY_1CPU(widget)->priv;
   GtkAllocation alloc;
   GdkWindowAttr attrs;
   guint attrs_mask;

   gtk_widget_set_realized(widget, TRUE);

   gtk_widget_get_allocation(widget, &alloc);

   attrs.x           = alloc.x;
   attrs.y           = alloc.y;
   attrs.width       = alloc.width;
   attrs.height      = alloc.height;
   attrs.window_type = GDK_WINDOW_CHILD;
   attrs.wclass      = GDK_INPUT_OUTPUT;
   attrs.event_mask  = gtk_widget_get_events(widget) 
   | GDK_EXPOSURE_MASK
   | GDK_STRUCTURE_MASK
   | GDK_ALL_EVENTS_MASK;

   attrs_mask = GDK_WA_X | GDK_WA_Y;

   priv->window = gdk_window_new(gtk_widget_get_parent_window(widget),
               &attrs, attrs_mask);
   gdk_window_set_user_data(priv->window, widget);
   gtk_widget_set_window(widget, priv->window);

   // widget->style = gtk_style_attach(gtk_widget_get_style( widget ),
   //                           priv->window);
   // gtk_style_set_background(widget->style, priv->window, GTK_STATE_NORMAL);
}
static void my_1cpu_preferred_height(GtkWidget *widget, 
    gint *min_height, gint *nat_height)
{
  *min_height = HEIGHT;
  *nat_height = HEIGHT;
}
static void my_1cpu_preferred_width(GtkWidget *widget, 
    gint *min_width, gint *nat_width)
{
  *min_width = WIDTH;
  *nat_width = WIDTH;
}

static void my_1cpu_size_allocate(GtkWidget *widget,
                 GtkAllocation *allocation) {
                     
   My1CpuPrivate *priv;

   priv = MY_1CPU(widget)->priv;

   gtk_widget_set_allocation(widget, allocation);

   if (gtk_widget_get_realized(widget)) {
       
      gdk_window_move_resize(priv->window, allocation->x, allocation->y,
          WIDTH, HEIGHT);
   }

   g_print("%d, %d\n", allocation->width, allocation->height);
}

static gboolean my_draw(GtkWidget *widget, cairo_t *cr) 
{               
  g_print("in draw\n");
   My1CpuPrivate *priv = MY_1CPU(widget)->priv;
   gint limit;
   gint i;


   cairo_translate(cr, 0, 7);

   cairo_set_source_rgb(cr, 0, 0, 0);
   cairo_paint(cr);

   limit = 20 - priv->percent / 5;
   
   for (i = 1; i <= 20; i++) {
       
      if (i > limit) {
         cairo_set_source_rgb(cr, 0.6, 1.0, 0);
      } else {
         cairo_set_source_rgb(cr, 0.2, 0.4, 0);
      }

      cairo_rectangle(cr, 8,  i * 4, 30, 3);
      cairo_rectangle(cr, 42, i * 4, 30, 3);
      cairo_fill(cr);
   }

   return TRUE;
}

static gboolean 
my_1cpu_configure_event(GtkWidget *widget, 
                      GdkEventConfigure *event)
{
  g_print("c %d, %d\n", event->width, event->height);

  return TRUE;
}




/* Public API */
GtkWidget *my_1cpu_new(int n, int a) {
    
   return(g_object_new(MY_TYPE_1CPU, NULL));
}

gdouble my_1cpu_get_percent(My1Cpu *cpu) {
    
   g_return_val_if_fail(MY_IS_1CPU(cpu), 0);

   return(cpu->priv->percent);
}

void my_1cpu_set_percent(My1Cpu *cpu, gdouble sel) {
    
   g_return_if_fail(MY_IS_1CPU(cpu));

   cpu->priv->percent = sel;
   gtk_widget_queue_draw(GTK_WIDGET(cpu));
}