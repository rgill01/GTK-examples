#include "mycpu.h"

void cb_changed(GtkRange *range, GtkWidget *cpu) {
   // GdkWindow *window;

   my_1cpu_set_percent(MY_1CPU(cpu), gtk_range_get_value(range));

   // window = gtk_widget_get_parent_window(GTK_WIDGET(range));
   // gtk_widget_queue_draw(GTK_WIDGET(window)); 
}

static void activate(GApplication *app, gpointer user_data)
{
   GtkWidget *window;
   GtkWidget *grid;
   GtkWidget *cpu;
   GtkWidget *scale;
   GtkAdjustment *adjustment;

   window = gtk_application_window_new(GTK_APPLICATION(app));
   gtk_window_set_title(GTK_WINDOW(window), "1CPU widget");
   gtk_window_set_default_size(GTK_WINDOW(window), 500, 500);
   gtk_container_set_border_width(GTK_CONTAINER(window), 15);

   grid = gtk_grid_new();
   cpu = my_1cpu_new(0,0);
   adjustment = gtk_adjustment_new(0, 0, 100, 5, 10, 0);
   scale = gtk_scale_new(GTK_ORIENTATION_VERTICAL, adjustment);
   gtk_widget_set_vexpand(scale, TRUE);
   gtk_scale_set_digits(GTK_SCALE(scale), 0);
   
   gtk_grid_attach(GTK_GRID(grid), cpu, 0, 0, 1, 1);
   gtk_grid_attach(GTK_GRID(grid), scale, 1, 0, 1, 1);

   gtk_container_add(GTK_CONTAINER(window), grid);
   
   g_signal_connect(scale, "value-changed", G_CALLBACK(cb_changed), cpu);
   
   gtk_widget_show_all(window);
}

int main(int argc, char *argv[]) 
{   
   GtkApplication *app;
   gint status;

   app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
   g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
   status = g_application_run(G_APPLICATION(app), argc, argv);
   g_object_unref(app);

   return status;
}