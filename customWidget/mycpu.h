 /* create a custom GTK+ widget */

#ifndef __MY_1CPU_H__
#define __MY_1CPU_H__

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

/* Standard GObject macros */

#define MY_TYPE_1CPU (my_1cpu_get_type())
G_DECLARE_FINAL_TYPE(My1Cpu, my_1cpu, MY, 1CPU, GtkWidget)
// #define MY_1CPU(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MY_TYPE_1CPU, My1Cpu))
// #define MY_1CPU_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), MY_TYPE_1CPU, My1CpuClass))
// #define MY_IS_1CPU(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), MY_TYPE_1CPU))
// #define MY_IS_1CPU_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), MY_TYPE_1CPU))
// #define MY_1CPU_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), MY_TYPE_1CPU, My1CpuClass))

// /* Type definition */
// typedef struct _My1Cpu        My1Cpu;
// typedef struct _My1CpuClass   My1CpuClass;
// typedef struct _My1CpuPrivate My1CpuPrivate;

// struct _My1Cpu {
    
//    GtkWidget parent;

//    /*< Private >*/
//    My1CpuPrivate *priv;
// };

// struct _My1CpuClass {
    
//    GtkWidgetClass parent_class;
// };

/* Public API */
// GType      my_cpu_get_type(void) G_GNUC_CONST;
GtkWidget *my_1cpu_new(int n, int a);

gdouble my_1cpu_get_percent(My1Cpu *cpu);
void    my_1cpu_set_percent(My1Cpu *cpu, gdouble sel);


G_END_DECLS

#endif /* include guard */