#include <cairo.h>
#include <gtk/gtk.h>
/*

Before the source is applied to the surface, it is filtered first. 
The mask is used as a filter. The mask determines where the source is applied 
and where not. Opaque parts of the mask allow to copy the source. 
Transparent parts do not let to copy the source to the surface. 

*/


struct {
	cairo_surface_t *image;
} glob;

static gboolean
on_draw_event(GtkWidget *widget,
							cairo_t *cr,
							gpointer user_data)
{
	cairo_set_source_rgba(cr, 0, 0, 0, 0.5);

	cairo_mask_surface(cr, glob.image, 0, 0);

	cairo_fill(cr);

	return FALSE;
}


static void 
activate (GtkApplication *app, 
					gpointer 				user_data)
{
	GtkWidget *window;
	GtkWidget *darea;
	gint width, height;

	glob.image = cairo_image_surface_create_from_png("image.png");
	width = cairo_image_surface_get_width(glob.image);
	height= cairo_image_surface_get_height(glob.image);

	window = gtk_application_window_new(app);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), width, height);
	gtk_window_set_title(GTK_WINDOW(window), "GTK Window");

	darea = gtk_drawing_area_new();
	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);
	gtk_container_add(GTK_CONTAINER(window), darea);

	gtk_widget_show_all(window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  
  g_object_unref (app);
  cairo_surface_destroy(glob.image);
	return status;
}