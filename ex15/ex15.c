#include <cairo.h>
#include <gtk/gtk.h>
/*

Clipping

Clipping is restricting of drawing to a certain area. This is done for efficiency reasons and to create interesting effects.

In the following example we will be clipping an image. 

*/


struct {
	cairo_surface_t *image;
} glob;

static gboolean
on_draw_event(GtkWidget *widget,
							cairo_t *cr,
							gpointer user_data)
{
	static gint pos_x = 128;
	static gint pos_y = 128;
	static gint radius = 40;
	static gint delta[] = { 3 , 3 };

	GtkWidget *win = gtk_widget_get_toplevel(widget);

	gint width, height;
	gtk_window_get_size(GTK_WINDOW(win), &width, &height);

	GRand *rand = g_rand_new();
	gint r = g_rand_int_range(rand, 0, 5);

	if (pos_x < 0 + radius)
		delta[0] = r + 5;
	else if (pos_x > width - radius)
		delta[0] = -(r + 5);

	r = g_rand_int_range(rand, 0, 4);
	if (pos_y < 0 + radius)
		delta[1] = r + 5;
	else if (pos_y > width - radius)
		delta[1] = -(r + 5);

	pos_x += delta[0];
	pos_y += delta[1];

	g_rand_free(rand);

	cairo_set_source_surface(cr, glob.image, 0, 0);
	cairo_arc(cr, pos_x, pos_y, radius, 0, 2*G_PI);
	cairo_clip(cr);
	cairo_paint(cr);

	return FALSE;
}

static gboolean
timer_handler(GtkWidget *widget)
{
	gtk_widget_queue_draw(widget);

	return TRUE;
}

static void 
activate (GtkApplication *app, 
					gpointer 				user_data)
{
	GtkWidget *window;
	GtkWidget *darea;
	gint width, height;

	glob.image = cairo_image_surface_create_from_png("image.png");
	width = cairo_image_surface_get_width(glob.image);
	height= cairo_image_surface_get_height(glob.image);

	window = gtk_application_window_new(app);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), width, height);
	gtk_window_set_title(GTK_WINDOW(window), "GTK Window");

	darea = gtk_drawing_area_new();
	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);
	gtk_container_add(GTK_CONTAINER(window), darea);

	g_timeout_add(100, (GSourceFunc) timer_handler, (gpointer) darea);

	gtk_widget_show_all(window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  cairo_surface_destroy(glob.image);
  return status;
}