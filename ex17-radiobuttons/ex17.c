#include <gtk/gtk.h>


static void
print_hello (GtkWidget *widget,
             gpointer   data)
{
  g_print("toggled\n");
  if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) ) {
    // g_print ("Hello World\n");
    g_print ("%s\n", gtk_button_get_label(GTK_BUTTON(widget)));
    // gtk_button_set_label(GTK_BUTTON(widget), "Hello (OB)");
  }
  else {
    gtk_button_set_label(GTK_BUTTON(widget), "_Hello");
  }
}

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

  GtkWidget  *box, *button1, *button2, *button3;
  box = gtk_button_box_new(GTK_ORIENTATION_VERTICAL);
  button1 = gtk_radio_button_new_with_mnemonic(NULL, "_Hello");
  gtk_container_add(GTK_CONTAINER(box), button1);
  button2 = gtk_radio_button_new_with_mnemonic_from_widget
    (GTK_RADIO_BUTTON(button1), "_Bye");
  gtk_container_add(GTK_CONTAINER(box), button2);
  button3 = gtk_radio_button_new_with_mnemonic_from_widget
    (GTK_RADIO_BUTTON(button1), "__Hi");
  gtk_container_add(GTK_CONTAINER(box), button3);

  g_signal_connect(button1, "clicked", G_CALLBACK(print_hello), 0);
  g_signal_connect(button2, "clicked", G_CALLBACK(print_hello), 0);
  g_signal_connect(button3, "clicked", G_CALLBACK(print_hello), 0);
  // g_signal_connect(button2, "toggled", G_CALLBACK(print_hello), 0);

  gtk_container_add(GTK_CONTAINER(window), box);
  gtk_widget_show_all (window);
}


int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
