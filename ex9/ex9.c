#include <gtk/gtk.h>

/* https://developer.gnome.org/gnome-devel-demos/unstable/menubar.vala.html.en 
 */

void on_response(GtkDialog *dialog, gint response_id, gpointer user_data)
{
	g_print("response is %d\n", response_id);

	gtk_widget_destroy(GTK_WIDGET(dialog));
}

static void show_dialog(GtkButton *button, gpointer user_data)
{
	GtkWindow *window = GTK_WINDOW(user_data);
	
	GtkWidget *dialog;
	GtkWidget *content_area;
	GtkWidget *label;

	dialog = gtk_dialog_new_with_buttons("Tutti Dialog", 
			window, GTK_DIALOG_USE_HEADER_BAR, "_Ok", GTK_RESPONSE_OK, NULL);

	content_area = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
	label = gtk_label_new("This is a dialog with a label in its content area");
	gtk_container_add(GTK_CONTAINER(content_area), label);

	gtk_widget_show_all(dialog); //to show the label

	g_signal_connect(GTK_DIALOG(dialog), "response", G_CALLBACK(on_response), NULL);
}

static void vscale_moved(GtkRange *range, gpointer user_data)
{
	GtkWidget *label = user_data;

	gdouble value = gtk_range_get_value(range);

	gchar *str = g_strdup_printf("Vertical scale is at %.0f", value);

	gtk_label_set_text(GTK_LABEL(label), str);
	
	g_free(str);
}


static void hscale_moved(GtkRange *range, gpointer user_data)
{
	GtkWidget *label = user_data;

	gdouble value = gtk_range_get_value(range);

	gchar *str = g_strdup_printf("Horizontal scale is at %.0f", value);

	gtk_label_set_text(GTK_LABEL(label), str);

	g_free(str);
}

static void copy_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	g_print("\"Copy\" activated\n");
}

static void paste_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	g_print("\"Paste\" activated\n");
}

static void shape_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	const gchar *answer = g_variant_get_string(parameter, NULL);
	g_print("Shape is set to %s.\n", answer);
	g_simple_action_set_state(simple, parameter);	
}

static void about_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	GtkWidget *about_dialog;
	about_dialog = gtk_about_dialog_new();

	const gchar *authors[] = {"GNOME bla bla team", NULL};
	const gchar *documenters[] = {"Rajan", NULL};

	/* fill about dialog */

	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(about_dialog), "Tutti program");
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(about_dialog), "Copyright \xc2\xa9 2012 GNOME Documentation Team");
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(about_dialog), authors);
	gtk_about_dialog_set_documenters(GTK_ABOUT_DIALOG(about_dialog), documenters);
	gtk_about_dialog_set_website_label(GTK_ABOUT_DIALOG(about_dialog), "tutti website");
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(about_dialog), "http://www.google.com");


	/* on response / close */

	g_signal_connect(GTK_DIALOG(about_dialog), "response", G_CALLBACK(on_response), NULL);

	gtk_widget_show(about_dialog);
}

static void activate(GApplication *app, gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *button;
	GtkWidget *h_scale, *v_scale, *h_label, *v_label, *grid;

	GtkAdjustment *hadjustment;
  GtkAdjustment *vadjustment; 


  GSimpleAction *copy_action, *paste_action, *shape_action, *about_action;

	window = gtk_application_window_new(GTK_APPLICATION(app));
	gtk_window_set_title(GTK_WINDOW(window), "tutti button with menubar");
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 250);
	gtk_container_set_border_width(GTK_CONTAINER(window), 5);

	/* copy action */

	copy_action = g_simple_action_new("copy", NULL);
	g_signal_connect(copy_action, "activate", G_CALLBACK(copy_callback), GTK_WINDOW(window));	
	g_action_map_add_action(G_ACTION_MAP(window), G_ACTION(copy_action));

	/* pase action */

	paste_action = g_simple_action_new("paste", NULL);
	g_signal_connect(paste_action, "activate", G_CALLBACK(paste_callback), 
		GTK_WINDOW(window));
	g_action_map_add_action(G_ACTION_MAP(window), G_ACTION(paste_action));

	/* shape action, has a state */

	GVariantType *type_string = g_variant_type_new("s");
	shape_action = g_simple_action_new_stateful("shape", type_string, 
		g_variant_new_string("line"));
	g_signal_connect(shape_action, "activate", G_CALLBACK(shape_callback), 
		GTK_WINDOW(window));
	g_action_map_add_action(G_ACTION_MAP(window), G_ACTION(shape_action));
	g_variant_type_free(type_string);

	/* about action */

  about_action = g_simple_action_new("about", NULL);
  g_signal_connect(about_action, "activate", G_CALLBACK(about_callback), 
  	GTK_WINDOW(window));
  g_action_map_add_action(G_ACTION_MAP(window), G_ACTION(about_action));



	/* slider related stuff */

	h_label = gtk_label_new("Move the scale handle");
	v_label = gtk_label_new("Move the scale handle");
	hadjustment = gtk_adjustment_new(0, 0, 100, 5, 10, 0);
	vadjustment = gtk_adjustment_new(50,0, 100, 5, 10, 0);

	h_scale = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, hadjustment);
	v_scale = gtk_scale_new(GTK_ORIENTATION_VERTICAL,   vadjustment);

	/* Allow it to expand horizontally (if there's space), and 
   * set the vertical alignment
   */
  gtk_widget_set_hexpand (h_scale, TRUE);
  gtk_widget_set_vexpand(v_scale, TRUE);

  gtk_scale_set_digits (GTK_SCALE (h_scale), 0); 
  gtk_widget_set_valign (h_scale, GTK_ALIGN_START);

  g_signal_connect(h_scale, "value-changed", G_CALLBACK(hscale_moved), h_label);
  g_signal_connect(v_scale, "value-changed", G_CALLBACK(vscale_moved), v_label);

	/* button related */

	button = gtk_button_new_with_mnemonic("_Click Me");
	g_signal_connect(GTK_BUTTON(button), "clicked", G_CALLBACK(show_dialog), 
		GTK_WINDOW(window));

	/* grid */

	grid = gtk_grid_new();
	gtk_grid_set_column_spacing (GTK_GRID (grid), 10);
  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);
  gtk_grid_attach (GTK_GRID (grid), h_scale, 0, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), v_scale, 1, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), h_label, 0, 1, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), v_label, 1, 1, 1, 1);
  gtk_grid_attach (GTK_GRID(grid), button, 0, 2, 2, 1);

  gtk_widget_set_tooltip_text(button, "a button");
  
  gtk_container_add (GTK_CONTAINER (window), grid);
  


	gtk_widget_show_all(window);
}

static void 
new_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	g_print("You clicked \"New\"\n");
}

static void
quit_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	GApplication *application = user_data;
	g_print("You clicked \"Quit\"\n");
	g_application_quit(application);
}

static void
state_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	const gchar *answer = g_variant_get_string(parameter, NULL);
	g_print("State is set to %s.\n", answer);
	g_simple_action_set_state(simple, parameter);
}

static void
awesome_callback(GSimpleAction *simple, GVariant *parameter, gpointer user_data)
{
	GVariant *action_state = g_action_get_state(G_ACTION(simple));
	gboolean active = g_variant_get_boolean(action_state);
	GVariant *new_state = g_variant_new_boolean(!active);

	g_simple_action_set_state(simple, new_state);

	if(active)
		g_print("You unchecked \"Awesome\"\n");
	else
		g_print("You checked \"Awesome\"\n");
}

static void startup(GApplication *app, gpointer user_data)
{
	GSimpleAction *new_action, *quit_action, *state_action, *awesome_action;

	GtkBuilder *builder;

	GError *error = NULL;

	/* new action */
	
	new_action = g_simple_action_new("new", NULL);
	g_signal_connect(new_action, "activate", G_CALLBACK(new_callback), app);
	g_action_map_add_action(G_ACTION_MAP(app), G_ACTION(new_action)); // why is this?

	/* quit action */

	quit_action = g_simple_action_new("quit", NULL);
	g_signal_connect(quit_action, "activate", G_CALLBACK(quit_callback), app);
	g_action_map_add_action(G_ACTION_MAP(app), G_ACTION(quit_action));

	/* state action */

	GVariantType *type_string2 = g_variant_type_new("s"); // what is this?
	state_action = g_simple_action_new_stateful("state", type_string2, 
		g_variant_new_string("off"));
	g_signal_connect(state_action, "activate", G_CALLBACK(state_callback), app);
	g_action_map_add_action(G_ACTION_MAP(app), G_ACTION(state_action));
	g_variant_type_free(type_string2);

	/* awesome action */

	awesome_action = g_simple_action_new_stateful("awesome", NULL, 
		g_variant_new_boolean(FALSE));
	g_signal_connect(awesome_action, "activate", G_CALLBACK(awesome_callback), app);
	g_action_map_add_action(G_ACTION_MAP(app), G_ACTION(awesome_action));

	/* builder GUI */

	builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, "menubar.ui", &error);
	if (error != NULL)
	{
		g_print("%s\n", error->message);
		g_error_free(error);
	}

	GObject *menubar = gtk_builder_get_object(builder, "menubar");
	gtk_application_set_menubar(GTK_APPLICATION(app), G_MENU_MODEL(menubar));

	GObject *appmenu = gtk_builder_get_object(builder, "appmenu");
	gtk_application_set_app_menu(GTK_APPLICATION(app), G_MENU_MODEL(appmenu));
}

int main(int argc, char *argv[])
{
	GtkApplication *app;
	gint status;
	
	app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	g_signal_connect(app, "startup", G_CALLBACK(startup), NULL);
	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	

	return status;
}

