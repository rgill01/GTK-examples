CC 			= gcc
CFLAGS 	= `pkg-config --cflags gtk+-3.0` -Wall -g -I ../clib -D USE_GLEW
LFLAGS 		= `pkg-config --libs gtk+-3.0` -lGL -lGLEW \
						-L/home/rgill/GIT/clib -Wl,-rpath=/home/rgill/GIT/clib -lmyclib

EX_OBJ  := $(patsubst %.c,%.out,$(wildcard ex*/*.c))

default:	examples

examples: $(EX_OBJ)

%.out: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LFLAGS)

clean:
	find . -name "*.out" -delete

