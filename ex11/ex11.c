#include <cairo.h>
#include <gtk/gtk.h>

struct {
	int count;
	double coordx[100];
	double coordy[100];
} glob;

static void 
do_drawing(cairo_t *cr, GtkWidget *widget)
{

	/* text */

	cairo_set_source_rgb(cr, 1, 1, 0);
	cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(cr, 40.0);
	cairo_move_to(cr, 0, 40.0);
	cairo_show_text(cr, "Hello World!");	

	/* lines */

	cairo_set_source_rgb(cr, 0,0,0);
	cairo_set_line_width(cr, 0.5);
	int i, j;
	for (i=0; i< glob.count; i++)
	{
		for (j=0; j< glob.count; j++)
		{
			cairo_move_to(cr, glob.coordx[i], glob.coordy[i]);
			cairo_line_to(cr, glob.coordx[j], glob.coordy[j]);
		}
	}

	glob.count = 0;
	cairo_stroke(cr);


	/* fill and stroke */

	GtkWidget *win = gtk_widget_get_toplevel(widget);
	int width, height;
	gtk_window_get_size(GTK_WINDOW(win), &width, &height);

	cairo_set_line_width(cr, 9);
	cairo_set_source_rgb(cr, 0.69, 0.19, 0);
	cairo_translate(cr, width/2, height/2);
	cairo_arc(cr, 0, 0, height/2, 0, 2*G_PI);
	cairo_stroke_preserve(cr);

	cairo_set_source_rgb(cr, 0.3, 0.4, 0.6);
	cairo_fill(cr);

	/* pen dash */

	cairo_set_source_rgba(cr, 0,0,0, 1);
	
	static const double dashed1[] = {4.0, 21.0, 2.0};
	static int len1 = sizeof(dashed1)/sizeof(dashed1[0]);
	static const double dashed2[] = {14, 6};
	static int len2 = sizeof(dashed2)/sizeof(dashed2[0]);
	static const double dashed3[] = {1};

	cairo_set_line_width(cr, 1.5);
	
	cairo_set_dash(cr, dashed1, len1, 0);
	cairo_move_to(cr, 40, 30);
	cairo_line_to(cr, 200, 30);
	cairo_stroke(cr);

	cairo_set_dash(cr, dashed2, len2, 1);
	cairo_move_to(cr, 40, 40);
	cairo_line_to(cr, 200, 40);
	cairo_stroke(cr);

	cairo_set_dash(cr, dashed3, 1, 0);
	cairo_move_to(cr, 40, 50);
	cairo_line_to(cr, 200, 50);
	cairo_stroke(cr);

	cairo_translate(cr, -width/2, -height/2);

	/* basic shapes */

  cairo_set_source_rgb(cr, 0.6, 0.6, 0.6);
  cairo_set_line_width(cr, 1);

  cairo_rectangle(cr, 20, 20, 120, 80);
  cairo_rectangle(cr, 180, 20, 80, 80);
  cairo_stroke_preserve(cr);
  cairo_fill(cr);

  cairo_set_source_rgb(cr, 0, 0.3, 0.0);
  cairo_arc(cr, 330, 60, 40, 0, 2*G_PI);
  cairo_stroke_preserve(cr);
  cairo_fill(cr);

	cairo_set_source_rgb(cr, 1, 0, 0.5);
  cairo_arc(cr, 90, 160, 40, G_PI/4, G_PI);
  cairo_close_path(cr);
  cairo_stroke_preserve(cr);
  cairo_fill(cr);

  cairo_translate(cr, 220, 180);
  cairo_scale(cr, 1, 0.7);
  cairo_arc(cr, 0, 0, 50, 0, 2*G_PI);
  cairo_stroke_preserve(cr);
  cairo_fill(cr);	

  /* transparency */

  for ( i = 1; i <= 10; i++) {
      cairo_set_source_rgba(cr, 0, 0, 1, i*0.1);
      cairo_rectangle(cr, 50*i, 20, 40, 40);
      cairo_fill(cr);  
  } 

}

static gboolean 
on_draw_event(GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
	do_drawing(cr, widget);

	return FALSE;
}

static gboolean
clicked(GtkWidget *widget, 
				GdkEventButton *event,
				gpointer user_data)
{
	if (event->button == 1)
	{
		glob.coordx[glob.count] 	= event->x;
		glob.coordy[glob.count++] = event->y;
	}

	if (event->button == 3)
	{
		gtk_widget_queue_draw(widget);
	}

	return TRUE;
}


static void 
activate (GtkApplication *app, 
					gpointer 				user_data)
{
	GtkWidget *window;
	GtkWidget *darea;

	window = gtk_application_window_new(app);
	gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 400, 90);
	gtk_window_set_title(GTK_WINDOW(window), "GTK Window");
	g_signal_connect(GTK_WINDOW(window), "button-press-event", G_CALLBACK(clicked), NULL);


	darea = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(window), darea);
	g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(on_draw_event), NULL);
	
	gtk_widget_show_all(window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}